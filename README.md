# Laptop_Price_Prediction
We have worked on a project called laptop price prediction by using a supervised model. It basically about predicting the price of several laptops depending on their various characteristics. We used numerous machine learning methods, but the decision tree was the most effective, with a 78% accuracy.

##  Model Deployment link
https://kelzangtshotsho-laptop-price-prediction-app-p2kxxs.streamlit.app/

***

